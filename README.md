CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * How it works
 * Support requests
 * Maintainers


INTRODUCTION
------------

Social Auth NeonCRM is a NeonCRM authentication integration for Drupal. It
is based on the Social Auth and Social API projects.

It adds to the site:
 * A new url: /user/login/neon-crm.
 * A settings form on /admin/config/social-api/social-auth/neon-crm.
 * A NeonCRM logo in the Social Auth Login block.


REQUIREMENTS
------------

This module requires the following modules:

 * Social Auth (https://drupal.org/project/social_auth)
 * Social API (https://drupal.org/project/social_api)


INSTALLATION
------------

 * Run composer to install the dependencies.
   composer require "drupal/social_auth_neon_crm:^2.0"

 * Install the dependencies: Social API and Social Auth.
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * Add your NeonCRM project OAuth information in
   Configuration » User Authentication » NeonCRM.

 * Place a Social Auth NeonCRM block in Structure » Block Layout.

 * If you already have a Social Auth Login block in the site, rebuild the cache.


HOW IT WORKS
------------

You can add a button or link anywhere on the site that points
to /user/login/neon-crm, so theming and customizing the button or link
is very flexible.

When the user opens the /user/login/neon-crm link, it automatically takes the
user to NeonCRM Accounts for authentication. NeonCRM then returns the user
to Drupal site. If we have an existing Drupal user with the same email address
provided by NeonCRM, that user is logged in. Otherwise a new Drupal user is
created.


SUPPORT REQUESTS
----------------

 * Before posting a support request, carefully read the installation
   instructions provided in module documentation page.

 * Before posting a support request, check Recent Log entries at
   admin/reports/dblog

 * Once you have done this, you can post a support request at module issue
   queue: https://www.drupal.org/project/issues/social_auth_neon_crm

 * When posting a support request, please inform if you were able to see any
   errors in the Recent Log entries.
