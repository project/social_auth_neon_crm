<?php

namespace Drupal\social_auth_neon_crm\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\social_auth\Form\SocialAuthSettingsForm;

/**
 * Settings form for Social Auth NeonCRM.
 */
class NeonCRMAuthSettingsForm extends SocialAuthSettingsForm
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'social_auth_neon_crm_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return array_merge(
            parent::getEditableConfigNames(),
            ['social_auth_neon_crm.settings']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('social_auth_neon_crm.settings');

        $form['neon_crm_settings'] = [
        '#type' => 'details',
        '#title' => $this->t('NeonCRM Client settings'),
        '#open' => true
        ];

        $form['neon_crm_settings']['custom_url'] = [
            '#type' => 'textfield',
            '#required' => true,
            '#title' => $this->t('Neon Custom URL'),
            '#default_value' => $config->get('custom_url'),
            '#description' => $this->t('Copy the custom URL used within NeonCRM, including HTTP/HTTPS.'),
            ];

        $form['neon_crm_settings']['api_key'] = [
        '#type' => 'textfield',
        '#required' => true,
        '#title' => $this->t('Neon API Key'),
        '#default_value' => $config->get('api_key'),
        '#description' => $this->t('Copy the NeonCRM Api Key here.'),
        ];

        $form['neon_crm_settings']['client_id'] = [
        '#type' => 'textfield',
        '#required' => true,
        '#title' => $this->t('Neon Client ID'),
        '#default_value' => $config->get('client_id'),
        '#description' => $this->t('Copy the NeonCRM Clinet ID here.'),
        ];

        $form['neon_crm_settings']['client_org'] = [
        '#type' => 'textfield',
        '#required' => true,
        '#title' => $this->t('Neon API Organization ID'),
        '#default_value' => $config->get('client_org'),
        '#description' => $this->t('Copy the NeonCRM Org ID here.'),
        ];

        $form['neon_crm_settings']['client_secret'] = [
        '#type' => 'textfield',
        '#required' => true,
        '#title' => $this->t('Neon API Client Secret'),
        '#default_value' => $config->get('client_secret'),
        '#description' => $this->t('Copy the NeonCRM Secret here.'),
        ];

        $form['neon_crm_settings']['authorized_redirect_url'] = [
        '#type' => 'textfield',
        '#disabled' => true,
        '#title' => $this->t('Authorized redirect URIs'),
        '#description' => $this->t('Copy this value to <em>Authorized redirect URIs</em> field of your NeonCRM App settings.'),
        '#default_value' => Url::fromRoute('social_auth_neon_crm.callback')->setAbsolute()->toString(),
        ];

        $form['neon_crm_settings']['advanced'] = [
        '#type' => 'details',
        '#title' => $this->t('Advanced settings'),
        '#open' => false,
        ];

        $form['neon_crm_settings']['advanced']['scopes'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Scopes for API call'),
        '#default_value' => $config->get('scopes'),
        '#description' => $this->t('Define any additional scopes to be requested, separated by a comma (e.g.: edit,history).<br> The scopes \'identity\' and \'read\' are added by default and always requested.<br>You can see the full list of valid endpoints and required scopes <a href="@scopes">here</a>.', ['@scopes' => 'https://www.reddit.com/dev/api/oauth']),
        ];

        $form['neon_crm_settings']['advanced']['endpoints'] = [
        '#type' => 'textarea',
        '#title' => $this->t('API calls to be made to collect data'),
        '#default_value' => $config->get('endpoints'),
        '#description' => $this->t(
            'Define the Endpoints to be requested when user authenticates with NeonCRM for the first time<br>
                                  Enter each endpoint in different lines in the format <em>endpoint</em>|<em>name_of_endpoint</em>.<br>
                                  <b>For instance:</b><br>
                                  /api/v1/me/trophies|user_trophies'
        ),
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $this->config('social_auth_neon_crm.settings')
            ->set('custom_url', trim($values['custom_url']))
            ->set('api_key', trim($values['api_key']))
            ->set('client_id', trim($values['client_id']))
            ->set('client_org', trim($values['client_org']))
            ->set('client_secret', trim($values['client_secret']))
            ->set('scopes', $values['scopes'])
            ->set('endpoints', $values['endpoints'])
            ->save();

        parent::submitForm($form, $form_state);
    }
}
