<?php

namespace Drupal\social_auth_neon_crm;

/**
 * Neon Api Interface.
 */
interface NeonApiInterface
{
    /**
     * Gets the session id.
     */
    public function getSessionId();

    /**
     * Returns event attendees list.
     */
    public function getEventAttendees($eventId, $pageSize);

    /**
     * Returns individual account info.
     */
    public function getIndividualAccount($accountId);
}
