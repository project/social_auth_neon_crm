<?php

namespace Drupal\social_auth_neon_crm\Plugin\Network;

use Drupal\social_auth\Plugin\Network\NetworkInterface;

/**
 * Defines the NeonCRM Auth interface.
 */
interface NeonCRMAuthInterface extends NetworkInterface
{
}
