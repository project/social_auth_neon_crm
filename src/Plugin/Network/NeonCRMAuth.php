<?php

namespace Drupal\social_auth_neon_crm\Plugin\Network;

use Drupal\social_api\Plugin\NetworkBase;
use Drupal\social_api\SocialApiException;
use Drupal\social_auth_neon_crm\Settings\NeonCRMAuthSettings;
use GuzzleHttp\Client;
use \Drupal\Core\Config\ConfigFactory;

/**
 * Defines a Network Plugin for Social Auth NeonCRM.
 *
 * @package Drupal\social_auth_neon_crm\Plugin\Network
 *
 * @Network(
 *   id = "social_auth_neon_crm",
 *   social_network = "NeonCRM",
 *   type = "social_auth",
 *   handlers = {
 *     "settings": {
 *       "class": "\Drupal\social_auth_neon_crm\Settings\NeonCRMAuthSettings",
 *       "config_id": "social_auth_neon_crm.settings"
 *     }
 *   }
 * )
 */
class NeonCRMAuth extends NetworkBase implements NeonCRMAuthInterface
{
    private $config;

    public function __construct()
    {

        $this->config = \Drupal::config('social_auth_neon_crm.settings');

    }

    /**
     * Sets the underlying SDK library.
     *
     * @return \Drupal\neon_api\NeonApi
     *   The initialized 3rd party library instance.
     *
     * @throws SocialApiException
     *   If the SDK library does not exist.
     */
    protected function initSdk()
    {
        //$class_name = '\Drupal\neon_api\NeonApi';
        $class_name = '\League\OAuth2\Client\Provider\GenericProvider';
        if (!class_exists($class_name)) {
            throw new SocialApiException(sprintf('The NeonCRM library for PHP not found. Class: %s.', $class_name));
        }

        /**
   * @var \Drupal\social_auth_neon_crm\Settings\NeonCRMAuthSettings $settings 
*/
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));

        if ($this->validateConfig($config)) {
            // All these settings are mandatory.
            $guzzle_settings = [
            'base_uri' => "https://app.neoncrm.com",
            ];

            $guzzyClient = new Client(
                [
                'defaults' => [
                \GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => 5,
                \GuzzleHttp\RequestOptions::ALLOW_REDIRECTS => true
                ],
                \GuzzleHttp\RequestOptions::VERIFY => false,
                ]
            );

            $host = \Drupal::request()->getSchemeAndHttpHost();
    
            $provider = new \League\OAuth2\Client\Provider\GenericProvider(
                [
                'client_id' => $config->getClientId(),
                'client_secret' => $config->getClientSecret(),
                'redirect_uri' => $host . '/user/login/neon-crm/callback',
                'urlAuthorize'            => $config->getCustomURL() . '/np/oauth/auth?redirect_uri='.$host.'/user/login/neon-crm/callback&response_type=code&client_id=' . $config->getClientId(),
                'urlAccessToken'          => 'https://app.neoncrm.com.org/np/oauth/token',
                'urlResourceOwnerDetails' => 'https://app.neoncrm.com.org/np/oauth/accounts'
                ]
            );

            $provider->setHttpClient($guzzyClient);

            return $provider;
        }

        return false;
    }

    /**
     * Checks that module is configured.
     *
     * @param \Drupal\social_auth_neon_crm\Settings\NeonCRMAuthSettings $settings
     *   The NeonCRM auth settings.
     *
     * @return bool
     *   True if module is configured.
     *   False otherwise.
     */
    protected function validateConfig(NeonCRMAuthSettings $settings)
    {
        $token = $settings->getClientId();
        $service = $settings->getClientSecret();
        if (!$token || !$service) {
            $this->loggerFactory
                ->get('social_auth_neon_crm')
                ->error('Define Client ID and Client Secret in module settings.');
            return false;
        }

        return true;
    }
}
