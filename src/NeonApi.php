<?php

namespace Drupal\social_auth_neon_crm;

use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;
use League\OAuth2\Client\Provider\AbstractProvider;

/**
 * Neon Api Service.
 */
class NeonApi extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * Domain
     *
     * @var string
     */
    public $domain = 'https://neonone.com';

    /**
     * Api domain
     *
     * @var string
     */
    public $apiDomain = 'https://app.neoncrm.com';

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));
        $host = \Drupal::request()->getSchemeAndHttpHost();
        return $config->getCustomURL() . '/np/oauth/auth?redirect_uri='.$host.'/user/login/neon-crm/callback&response_type=code&client_id=' . $config->getClientId();
    }

    /**
     * Get access token url to retrieve token
     *
     * @param array $paramsS
     *
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->apiDomain.'/np/oauth/token';
    }

    /**
     * Get provider url to fetch user details
     *
     * @param AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));
        if ($this->domain === $config->getCustomURL()) {
            return $this->apiDomain.'/np/oauth/apiAccount';
        }
        return $this->domain.'/np/oauth/apiAccount';
    }

    /**
     * Get the default scopes used by this provider.
     *
     * This should not be a complete list of all scopes, but the minimum
     * required for the provider user interface!
     *
     * @return array
     */
    protected function getDefaultScopes()
    {
        return [];
    }

    /**
     * Check a provider response for errors.
     *
     * @link   https://developer.github.com/v3/#client-errors
     * @link   https://developer.github.com/v3/oauth/#common-errors-for-the-access-token-request
     * @throws IdentityProviderException
     * @param  ResponseInterface $response
     * @param  string            $data     Parsed response data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if ($response->getStatusCode() >= 400) {
            throw IdentityProviderException::getResponseBody($response, $data);
        } elseif (isset($data['error'])) {
            throw IdentityProviderException::getResponseBody($response, $data);
        }
    }

    /**
     * Generate a user object from a successful user details request.
     *
     * @param  array       $response
     * @param  AccessToken $token
     * @return League\OAuth2\Client\Provider\ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        $user = new GenericResourceOwner($response);

        return $user->setDomain($this->domain);
    }

    /**
     * Returns the current value of the state parameter.
     *
     * This can be accessed by the redirect handler during authorization.
     *
     * @return string
     */
    public function getState()
    {
        return true;  //Required for validation check but not issued by NeonCRM with Guzzle
    }
}