<?php

namespace Drupal\social_auth_neon_crm\Settings;

/**
 * Defines an interface for Social Auth NeonCRM settings.
 */
interface NeonCRMAuthSettingsInterface
{
    /**
     * Gets the Custom URL.
     *
     * @return string
     *   The Custom URL.
     */
    public function getCustomURL();

    /**
     * Gets the Api Key.
     *
     * @return string
     *   The Api Key.
     */
    public function getApiKey();


    /**
     * Gets the client ID.
     *
     * @return string
     *   The client ID.
     */
    public function getClientId();

    /**
     * Gets the client secret.
     *
     * @return string
     *   The client secret.
     */
    public function getClientSecret();

    /**
     * Gets the client org.
     *
     * @return string
     *   The client org.
     */
    public function getClientOrg();

    /**
     * Gets the data Point defined the settings form page.
     *
     * @return string
     *   Comma-separated scopes.
     */
    public function getScopes();

    /**
     * Gets the User Agent string input in settings form page.
     *
     * @return string
     *   User agent string.
     */
    public function getUserAgentString();
}
