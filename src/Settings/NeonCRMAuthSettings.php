<?php

namespace Drupal\social_auth_neon_crm\Settings;

use Drupal\social_api\Settings\SettingsBase;

/**
 * Defines methods to get Social Auth NeonCRM settings.
 */
class NeonCRMAuthSettings extends SettingsBase implements NeonCRMAuthSettingsInterface
{
    /**
     * Custom URL.
     *
     * @var string
     */
    protected $customURL;
    /**
     * Api Key.
     *
     * @var string
     */
    protected $apiKey;
    /**
     * Client secret.
    /**
     * Client ID.
     *
     * @var string
     */
    protected $clientId;
    /**
     * Client secret.
     *
     * @var string
     */
    protected $clientSecret;
    /**
     * The data point to be collected.
     *
     * @var string
     */
    protected $scopes;
    /**
     * User Agent string.
     *
     * @var string
     */
    protected $userAgentString;
     /**
      * Client ORG.
      *
      * @var string
      */
    protected $clientOrg;

    /**
     * {@inheritdoc}
     */
    public function getCustomURL()
    {
        if (!$this->customURL) {
            $this->customURL = $this->config->get('custom_url');
        }
        return $this->customURL;
    }

    /**
     * {@inheritdoc}
     */
    public function getApiKey()
    {
        if (!$this->apiKey) {
            $this->apiKey = $this->config->get('api_key');
        }
        return $this->apiKey;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientId()
    {
        if (!$this->clientId) {
            $this->clientId = $this->config->get('client_id');
        }
        return $this->clientId;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientSecret()
    {
        if (!$this->clientSecret) {
            $this->clientSecret = $this->config->get('client_secret');
        }
        return $this->clientSecret;
    }

    /**
     * {@inheritdoc}
     */
    public function getClientOrg()
    {
        if (!$this->clientOrg) {
            $this->clientOrg = $this->config->get('client_org');
        }
        return $this->clientOrg;
    }

    /**
     * {@inheritdoc}
     */
    public function getScopes()
    {
        if (!$this->scopes) {
            $this->scopes = $this->config->get('scopes');
        }
        return $this->scopes;
    }

    /**
     * {@inheritdoc}
     */
    public function getUserAgentString()
    {
        if (!$this->userAgentString) {
            $this->userAgentString = $this->config->get('user_agent_string');
        }
        return $this->userAgentString;
    }
}
