<?php

namespace Drupal\social_auth_neon_crm;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_auth\AuthManager\OAuth2Manager;
use GuzzleHttp\Client;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\social_auth_neon_crm\Settings\NeonCRMAuthSettings;

/**
 * Contains all the logic for NeonCRM OAuth2 authentication.
 */
class NeonCRMAuthManager extends OAuth2Manager
{
    /**
     * Constructor.
     *
     * @param \Drupal\Core\Config\ConfigFactory                 $config_factory
     *   Used for accessing configuration object factory.
     * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
     *   The logger factory.
     * @param \Symfony\Component\HttpFoundation\RequestStack    $request_stack
     *   Used to get the authorization code from the callback request.
     */
    public function __construct(ConfigFactory $config_factory,
        LoggerChannelFactoryInterface $logger_factory,
        RequestStack $request_stack
    ) {

        parent::__construct(
            $config_factory->get('social_auth_neon_crm.settings'),
            $logger_factory,
            $this->request = $request_stack->getCurrentRequest()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate()
    {
        $host = \Drupal::request()->getSchemeAndHttpHost();
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));

        $resources = [
            'client_id' => $config->getClientId(),
            'client_secret' => $config->getClientSecret(),
            'redirect_uri' => $config->getCustomURL() . '/np/oauth/auth?redirect_uri=' . $host . '/user/login/&response_type=code&client_id=' . $config->getClientId(),
            'code' => $this->request->query->get('code'),
            'grant_type' => 'authorization_code',
        ];

        $helper = new Client(
            [
            'defaults' => [
              \GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => 5,
              \GuzzleHttp\RequestOptions::ALLOW_REDIRECTS => true
            ],
            \GuzzleHttp\RequestOptions::VERIFY => false,
            ]
        );

        $res = $helper->request(
            'POST',
            'https://app.neoncrm.com/np/oauth/token',
            ['form_params' => $resources],
            ['headers' => ['Content-Type' => 'application/x-www-form-urlencoded']]
        );

        $code = $res->getStatusCode(); // 200
        $reason = $res->getReasonPhrase(); // OK
        $access_token = $res->getBody()->getContents();
        $_SESSION['access_token'] = $access_token;

        try {
            $this->setAccessToken($access_token);
        } catch (IdentityProviderException $e) {
            $this->loggerFactory->get('social_auth_neon_crm')
                ->error('There was an error during authentication. Exception: ' . $e->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getUserInfo()
    {
        $storage = json_decode($_SESSION['access_token']);
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));

        $helper = new Client();
        $res = $helper->request(
            'GET',
            'https://api.neoncrm.com/v2/accounts/' . $storage->access_token,
            ['auth' => [$config->getClientOrg(), $config->getApiKey()]],
            ['headers' => ['Content-Type' => 'application/x-www-form-urlencoded']]
        );

        $code = $res->getStatusCode(); // 200
        $reason = $res->getReasonPhrase(); // OK
        $user = $res->getBody()->getContents();

        return json_decode($user);
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthorizationUrl()
    {
        $host = \Drupal::request()->getSchemeAndHttpHost();
        $config = new NeonCRMAuthSettings(\Drupal::configFactory()->get('social_auth_neon_crm.settings'));
        return $config->getCustomURL() . "/np/oauth/auth?response_type=code&client_id=".$config->getClientId()."&redirect_uri=" . $host . "/user/login/neon-crm/callback";
        $scopes = ['user:email'];

        $extra_scopes = $this->getScopes();
        if ($extra_scopes) {
            $scopes = array_merge($scopes, explode(',', $extra_scopes));
        }

        // Returns the URL where user will be redirected.
        return $this->client->getAuthorizationUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function requestEndPoint($method, $path, $domain = null, array $options = [])
    {
        if (!$domain) {
            $domain = 'https://api.neoncrm.com/v2/';
        }

        $url = $domain . $path;

        $request = $this->client->getAuthenticatedRequest($method, $url, $this->getAccessToken(), $options);

        try {
            return $this->client->getParsedResponse($request);
        } catch (IdentityProviderException $e) {
            $this->loggerFactory->get('social_auth_neon_crm')->error('There was an error when requesting ' . $url . '. Exception: ' . $e->getMessage());
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getState()
    {
        return $this->client->getState();
    }
}
