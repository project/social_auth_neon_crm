<?php

namespace Drupal\social_auth_neon_crm\Controller;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\Controller\OAuth2ControllerBase;
use Drupal\social_auth\SocialAuthDataHandler;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_neon_crm\NeonCRMAuthManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Social Auth NeonCRM routes.
 */
class NeonCRMAuthController extends OAuth2ControllerBase
{

    /**
     * The NeonCRM authentication manager.
     *
     * @var \Drupal\social_auth_neon_crm\NeonCRMAuthManager
     */
    protected $providerManager;

    /**
     * NeonCRMAuthController constructor.
     *
     * @param \Drupal\Core\Messenger\MessengerInterface       $messenger
     *   The messenger service.
     * @param \Drupal\social_api\Plugin\NetworkManager        $network_manager
     *   Used to get an instance of social_auth_neon_crm network plugin.
     * @param \Drupal\social_auth\User\UserAuthenticator      $user_authenticator
     *   Manages user login/registration.
     * @param \Drupal\social_auth_neon_crm\NeonCRMAuthManager $neoncrm_manager
     *   Used to manage authentication methods.
     * @param \Symfony\Component\HttpFoundation\RequestStack  $request
     *   Used to access GET parameters.
     * @param \Drupal\social_auth\SocialAuthDataHandler       $data_handler
     *   The Social Auth data handler.
     * @param \Drupal\Core\Render\RendererInterface           $renderer
     *   Used to handle metadata for redirection to authentication URL.
     */
    public function __construct(MessengerInterface $messenger,
        NetworkManager $network_manager,
        UserAuthenticator $user_authenticator,
        NeonCRMAuthManager $neoncrm_manager,
        RequestStack $request,
        SocialAuthDataHandler $data_handler,
        RendererInterface $renderer
    ) {

        parent::__construct(
            'Social Auth NeonCRM', 'social_auth_neon_crm',
            $messenger, $network_manager, $user_authenticator,
            $neoncrm_manager, $request, $data_handler, $renderer
        );
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('messenger'),
            $container->get('plugin.network.manager'),
            $container->get('social_auth.user_authenticator'),
            $container->get('social_auth_neon_crm.manager'),
            $container->get('request_stack'),
            $container->get('social_auth.data_handler'),
            $container->get('renderer')
        );
    }

    /**
     * Response for path 'user/login/neon-crm/callback'.
     *
     * NeonCRM returns the user here after user has authenticated.
     */
    public function callback()
    {

        // Checks if there was an authentication error.
        $redirect = $this->checkAuthError();
        if ($redirect) {
            return $redirect;
        }

        /* @var \Morgann\OAuth2\Client\NeonCRM\Provider\NeonCRM[ResourceOwner]??|null $profile */
        $profile = $this->processCallback();

        // If authentication was successful.
        if ($profile !== null) {

            $user = $profile->individualAccount;
            $profile = $profile->individualAccount->primaryContact;
            $storage = json_decode($_SESSION['access_token']);
            $data = $this->userAuthenticator->checkProviderIsAssociated($profile->contactId) ? null : $this->providerManager->getExtraDetails();
            if (property_exists($user, 'login')) {
                $login = $user->login;
                $username = !empty($login) && property_exists($login, 'username') ? $user->login->username : $profile->firstName . ' ' . $profile->lastName;
            }
            $_SESSION['user_profile'] = $profile;

            // If user information could be retrieved.
            return $this->userAuthenticator->authenticateUser(
                $username,
                $profile->email1,
                $profile->contactId,
                $storage->access_token,
                null,
                null
            );
        }

        return $this->redirect('user.login');
    }
}
